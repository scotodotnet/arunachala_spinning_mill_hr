﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class UnpaidSalaryCoverReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();


    string Date1;
    string Date2;
    string RptName = "";
    DataTable dt = new DataTable();

   
    DataRow dtRow;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Unpaid Salary Cover Report";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Date1 = Request.QueryString["FromDate"].ToString();
            Date2 = Request.QueryString["ToDate"].ToString();
            RptName = Request.QueryString["RptName"].ToString();

            if (RptName == "UNPAID SALARY COVER REPORT")
            {
                UnpaidSalaryCover();
            }
            else if (RptName == "COVER ABSTRACT REPORT")
            {
                CoverAbstractReport();
            }
        }
    }

    public void UnpaidSalaryCover()
    {
        string Time_Str = "0";
        string Time_Amt = "0";
        string Cash_Str = "0";
        string Cash_Amt = "0";

        DataTable DT_Unpaid = new DataTable();
        DataTable DT_Time = new DataTable();
        DataTable DT_Cash = new DataTable();
        DataTable DT = new DataTable();

        SSQL = "Select SP.ExisistingCode as TokenNo,SP.EmpName,EM.DeptName,EM.Designation,SP.Voucher_Amt ";
        SSQL = SSQL + " from [Arunachalam_Payroll]..SalaryVoucherPayment SP inner join Employee_Mst EM on SP.MachineNo=EM.MachineID";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Convert(datetime,SP.FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,SP.ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "Select Count(ExisistingCode) as Strength,isnull(sum(Voucher_Amt),0) as Amt from [Arunachalam_Payroll]..SalaryVoucherPayment";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " And Status='1' and Cover_Conform='1' and (Cover_Conform_Date!='' or Cover_Conform_Date is not null)";
        DT_Time = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Time.Rows.Count != 0)
        {
            Time_Str = DT_Time.Rows[0]["Strength"].ToString();
            Time_Amt = DT_Time.Rows[0]["Amt"].ToString();
        }


        SSQL = "Select Count(ExisistingCode) as Strength,isnull(sum(Voucher_Amt),0) as Amt from [Arunachalam_Payroll]..SalaryVoucherPayment";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " And Status='1' and (Cover_Conform='' or Cover_Conform is null) and (Cover_Conform_Date='' or Cover_Conform_Date is null)";
        DT_Cash = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Cash.Rows.Count != 0)
        {
            Cash_Str = DT_Cash.Rows[0]["Strength"].ToString();
            Cash_Amt = DT_Cash.Rows[0]["Amt"].ToString();
        }


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = "";
        if (dt.Rows.Count != 0)
        {
            name = dt.Rows[0]["CompName"].ToString();
        }


        if (DT.Rows.Count != 0)
        {
            SSQL = "Select SP.ExisistingCode as ExCode,SP.EmpName as Name,EM.DeptName as Dept,EM.Designation as Category,SP.Voucher_Amt as Total ";
            SSQL = SSQL + " from [Arunachalam_Payroll]..SalaryVoucherPayment SP inner join Employee_Mst EM on SP.MachineNo=EM.MachineID";
            SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "' And SP.Status='0'";
            SSQL = SSQL + " And Convert(datetime,SP.FromDate,103) =convert(datetime,'" + Date1 + "',103)";
            SSQL = SSQL + " And Convert(datetime,SP.ToDate,103) =convert(datetime,'" + Date2 + "',103)";
            DT_Unpaid = objdata.RptEmployeeMultipleDetails(SSQL);


            ds.Tables.Add(DT_Unpaid);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Unpaid_Salary_Cover_Report.rpt"));



            if (Date1 != "" && Date2 != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Date1.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + Date2.ToString() + "'";
            }

            report.DataDefinition.FormulaFields["Time_Str"].Text = "'" + Time_Str.ToString() + "'";
            report.DataDefinition.FormulaFields["Time_Amt"].Text = "'" + Time_Amt.ToString() + "'";

            report.DataDefinition.FormulaFields["Cash_Str"].Text = "'" + Cash_Str.ToString() + "'";
            report.DataDefinition.FormulaFields["Cash_Amt"].Text = "'" + Cash_Amt.ToString() + "'";

            report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + SessionLcode.ToString() + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }

    public void CoverAbstractReport()
    {
        string Time_Str = "0";
        string Time_Amt = "0";
        string Cash_Str = "0";
        string Cash_Amt = "0";

        DataTable DT_Unpaid = new DataTable();
        DataTable DT_Time = new DataTable();
        DataTable DT_Cash = new DataTable();
        DataTable DT = new DataTable();

        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("DayWages");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("EmpCode");


        //REGULAR
        SSQL = "Select count(SP.MachineNo) as EmpCount,isnull(Sum(SP.Voucher_Amt),0) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.Wages='REGULAR' ";
        SSQL = SSQL + " And Convert(datetime,SP.FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,SP.ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select count(SP.MachineNo) as EmpCount,Sum(SP.Voucher_Amt) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Status='1' And EM.Wages='REGULAR' ";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " and Cover_Conform='1' and (Cover_Conform_Date!='' or Cover_Conform_Date is not null)";
        DT_Time = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select count(SP.MachineNo) as EmpCount,Sum(SP.Voucher_Amt) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Status='1' And EM.Wages='REGULAR' ";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " And (Cover_Conform='' or Cover_Conform is null) and (Cover_Conform_Date='' or Cover_Conform_Date is null)";
        DT_Cash = objdata.RptEmployeeMultipleDetails(SSQL);


        if (DT.Rows.Count != 0 || DT_Time.Rows.Count != 0 || DT_Cash.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Dept"] = "REGULAR";

            if (DT.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DayWages"] = DT.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = DT.Rows[0]["SalAmt"].ToString();
            }

            if (DT_Time.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Shift"] = DT_Time.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Category"] = DT_Time.Rows[0]["SalAmt"].ToString();
            }

            if (DT_Cash.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SubCategory"] = DT_Cash.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpCode"] = DT_Cash.Rows[0]["SalAmt"].ToString();
            }
        }



        //HOSTEL
        SSQL = "Select count(SP.MachineNo) as EmpCount,isnull(Sum(SP.Voucher_Amt),0) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.Wages='HOSTEL' ";
        SSQL = SSQL + " And Convert(datetime,SP.FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,SP.ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select count(SP.MachineNo) as EmpCount,Sum(SP.Voucher_Amt) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Status='1' And EM.Wages='HOSTEL' ";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " and Cover_Conform='1' and (Cover_Conform_Date!='' or Cover_Conform_Date is not null)";
        DT_Time = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select count(SP.MachineNo) as EmpCount,Sum(SP.Voucher_Amt) as SalAmt from Employee_Mst EM";
        SSQL = SSQL + " inner join [Arunachalam_Payroll]..SalaryVoucherPayment SP on EM.MachineID=SP.MachineNo";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Ccode='" + SessionCcode + "' And SP.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SP.Status='1' And EM.Wages='HOSTEL' ";
        SSQL = SSQL + " And Convert(datetime,FromDate,103) =convert(datetime,'" + Date1 + "',103)";
        SSQL = SSQL + " And Convert(datetime,ToDate,103) =convert(datetime,'" + Date2 + "',103)";
        SSQL = SSQL + " And (Cover_Conform='' or Cover_Conform is null) and (Cover_Conform_Date='' or Cover_Conform_Date is null)";
        DT_Cash = objdata.RptEmployeeMultipleDetails(SSQL);


        if (DT.Rows.Count != 0 || DT_Time.Rows.Count != 0 || DT_Cash.Rows.Count != 0)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Dept"] = "HOSTEL";

            if (DT.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DayWages"] = DT.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total"] = DT.Rows[0]["SalAmt"].ToString();
            }

            if (DT_Time.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Shift"] = DT_Time.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Category"] = DT_Time.Rows[0]["SalAmt"].ToString();
            }

            if (DT_Cash.Rows.Count != 0)
            {
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SubCategory"] = DT_Cash.Rows[0]["EmpCount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpCode"] = DT_Cash.Rows[0]["SalAmt"].ToString();
            }
        }


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = "";
        if (dt.Rows.Count != 0)
        {
            name = dt.Rows[0]["CompName"].ToString();
        }


        if (AutoDTable.Rows.Count != 0)
        {
            
            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Cover_Abstract_Report.rpt"));



            if (Date1 != "" && Date2 != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + Date1.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + Date2.ToString() + "'";
            }
            
            report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + SessionLcode.ToString() + "'";


            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }

}
