﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class AdvancePayReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string query;
    string Get_Report_Type = "0";
    DataTable ds1 = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt_ID = new DataTable();
    DataTable AutoDataTable = new DataTable();
    static string CmpName;
    static string Cmpaddress;
    string ExistingCode = "";
    string WagesType = "";
    string id = "";
    ReportDocument rd = new ReportDocument();
    DataSet ds = new DataSet();
    string mydate;
    string EmpNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Salary Cover Summary MD";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Get_Report_Type = Request.QueryString["RptNam"].ToString();
            if (Get_Report_Type == "SingleEmployeeAdvance")
            {



                AutoDataTable.Columns.Add("EmpNo");
                AutoDataTable.Columns.Add("ExisistingCode");
                AutoDataTable.Columns.Add("DepartmentNm");
                AutoDataTable.Columns.Add("Designation");
                AutoDataTable.Columns.Add("DOJ");
                AutoDataTable.Columns.Add("Amount");
                AutoDataTable.Columns.Add("AD_Date");
                AutoDataTable.Columns.Add("Months");
                AutoDataTable.Columns.Add("Paid_Amount");

                AutoDataTable.Columns.Add("img", System.Type.GetType("System.Byte[]"));


                ExistingCode = Request.QueryString["ExistingCode"].ToString();
                WagesType = Request.QueryString["Wages"].ToString();
                id = Request.QueryString["id"].ToString();


                query = "Select  ID from [Arunachalam_Payroll].. AdvancePayment where Ccode='" + SessionCcode + "' ";
                query = query + " and Lcode='" + SessionLcode + "' and ExistNo='" + ExistingCode + "'";
                dt_ID = objdata.RptEmployeeMultipleDetails(query);

                if (dt_ID.Rows.Count != 0)
                {
                    string Advance_ID_Get = dt_ID.Rows[0]["ID"].ToString();

                    query = " Select ED.EmpNo,ED.ExistingCode as ExisistingCode ,ED.FirstName as EmpName,ED.DeptName as DepartmentNm,";
                    query = query + " ED.Designation,Convert(varchar,ED.DOJ,105) as DOJ,AD.Amount,Convert(varchar,AD.TransDate,105) as ADH_Date,Convert(varchar,ADH.TransDate,105) as AD_Date,";
                    query = query + "  ADH.Months,ADH.Amount as Paid_Amount,'' as img from Employee_Mst  ED";
                    query = query + " inner join [Arunachalam_Payroll]..AdvancePayment AD on AD.EmpNo=ED.EmpNo";
                    query = query + "  inner join [Arunachalam_Payroll]..Advancerepayment ADH on ADH.EmpNo=ED.EmpNo";
                    query = query + " where ED.Compcode='" + SessionCcode + "' And ED.Loccode='" + SessionLcode + "'";
                    query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                    query = query + " And ADH.Ccode='" + SessionCcode + "' And ADH.Lcode='" + SessionLcode + "'";
                    query = query + " And AD.ID='" + id + "' And ADH.AdvanceId='" + id + "'";
                    query = query + " Order by ADH.TransDate Asc";


                    ds1 = objdata.RptEmployeeMultipleDetails(query);



                }




                if (ds1.Rows.Count != 0)
                {


                    mydate = Convert.ToDateTime(ds1.Rows[0]["ADH_Date"]).ToString("dd-MM-yyyy");
                    EmpNo = ds1.Rows[0]["EmpNo"].ToString();
                    string form = ".jpg";
                    string UNIT_Folder = "";
                    byte[] imageByteData_Photo = new byte[0];
                    if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/AdvanceCopy"; }
                    if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/AdvanceCopy"; }
                    if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/AdvanceCopy"; }
                    if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV/AdvanceCopy"; }

                    string path_1 = "~/" + UNIT_Folder + "/" + mydate + "-" + EmpNo + form;
                    string Photo_Path = "";
                    Photo_Path = HttpContext.Current.Request.MapPath(path_1);
                    if (!System.IO.File.Exists(Photo_Path))
                    {
                        //imageByteData_Photo = System.IO.File.ReadAllBytes(path);
                        Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                    }
                    imageByteData_Photo = System.IO.File.ReadAllBytes(Photo_Path);
                    for (int i = 0; i < ds1.Rows.Count; i++)
                    {
                        ds1.Rows[i]["img"] = imageByteData_Photo;
                    }

                    query = " Select Cname,Location,Address1,Address2,Location,Pincode from [Arunachalam_Payroll]..AdminRights ";
                    query = query + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                    }

                    for (int iRow = 0; iRow < ds1.Rows.Count; iRow++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();



                        AutoDataTable.Rows[iRow]["EmpNo"] = ds1.Rows[iRow]["EmpNo"].ToString();
                        AutoDataTable.Rows[iRow]["ExisistingCode"] = ds1.Rows[iRow]["ExisistingCode"].ToString();
                        AutoDataTable.Rows[iRow]["DepartmentNm"] = ds1.Rows[iRow]["DepartmentNm"].ToString();
                        AutoDataTable.Rows[iRow]["Designation"] = ds1.Rows[iRow]["Designation"].ToString();
                        AutoDataTable.Rows[iRow]["DOJ"] = ds1.Rows[iRow]["DOJ"].ToString();
                        AutoDataTable.Rows[iRow]["Amount"] = ds1.Rows[iRow]["Amount"].ToString();
                        AutoDataTable.Rows[iRow]["AD_Date"] = ds1.Rows[iRow]["AD_Date"].ToString();
                        AutoDataTable.Rows[iRow]["Months"] = ds1.Rows[iRow]["Months"].ToString();
                        AutoDataTable.Rows[iRow]["Paid_Amount"] = ds1.Rows[iRow]["Paid_Amount"].ToString();
                        AutoDataTable.Rows[iRow]["img"] = imageByteData_Photo;
                    }


                    ds.Tables.Add(AutoDataTable);

                    rd.Load(Server.MapPath("Payslip/Advance_History_Single_Employee.rpt"));
                    
                    rd.Database.Tables[0].SetDataSource(ds.Tables[0]);


                    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";


                    //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";
                    rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                    CrystalReportViewer1.ReportSource = rd;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
                }
            }

            if (Get_Report_Type == "AllEmployeeAdvance")
            {
                string Wages = Request.QueryString["Wages"].ToString();

                query = " Select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName ,ED.DeptName as DepartmentNm ,";
                query = query + " ED.Designation,Convert(varchar,ED.DOJ,105) as DOJ,AD.Amount,AD.BalanceAmount";
                query = query + " from Employee_Mst ED  inner join [Arunachalam_Payroll]..AdvancePayment AD on AD.EmpNo=ED.EmpNo";
                query = query + " where ED.Compcode='" + SessionCcode + "' And ED.Loccode='" + SessionLcode + "'";
                query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And ED.Wages='" + Wages + "' And AD.Completed='N' ";
                query = query + " Order by ED.ExistingCode Asc";

                ds1 = objdata.RptEmployeeMultipleDetails(query);


                if (ds1.Rows.Count != 0)
                {

                    query = " Select Cname,Location,Address1,Address2,Location,Pincode from [Arunachalam_Payroll]..AdminRights ";
                    query = query + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                    }
                    ds.Tables.Add(ds1);

                    rd.Load(Server.MapPath("Payslip/Advance_History_All_Employee.rpt"));
                    
                    rd.Database.Tables[0].SetDataSource(ds.Tables[0]);


                    rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                    rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                    rd.DataDefinition.FormulaFields["EmployeeType"].Text = "'" + Wages + "'";

                    rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                    CrystalReportViewer1.ReportSource = rd;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
                }
            }

        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
