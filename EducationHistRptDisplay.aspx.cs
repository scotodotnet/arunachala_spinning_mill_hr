﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class EducationHistRptDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();





    DataTable DataCells = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string Course; string Status;

    ReportDocument report = new ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Recruitment Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();
            Course = Request.QueryString["Course"].ToString();
            Status = Request.QueryString["Status"].ToString();




            SSQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,Convert(varchar,EM.DOJ,103) as DOJ,";
            SSQL = SSQL + "ED.Pursuing,ED.University,ED.Course,ED.Department,ED.StartYear,ED.CompleteYear,";
            SSQL = SSQL + "(case ED.Complt_Chk when '1' then 'Completed' else 'Pursuing' end) as Status";
            SSQL = SSQL + " from Employee_Mst EM inner join Education_Details ED on EM.ExistingCode=ED.ExistingCode and EM.LocCode=ED.LocCode";
            SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
            SSQL = SSQL + " And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
            if (Course != "")
            {
                SSQL = SSQL + " And ED.Pursuing='" + Course + "'";
            }
            if (Status == "Completed")
            {
                SSQL = SSQL + " And ED.Complt_Chk='1'";
            }
            else if (Status == "Pursuing")
            {
                SSQL = SSQL + " And ED.Complt_Chk!='1'";
            }

            SSQL = SSQL + "order by EM.ExistingCode asc ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count != 0)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();


                ds.Tables.Add(dt);
                //ReportDocument report = new ReportDocument();

                report.Load(Server.MapPath("crystal/EducationHistoryReport.rpt"));

                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
                report.DataDefinition.FormulaFields["LocName"].Text = "'" + SessionLcode.ToString() + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
