﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class TraineeReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();

   
    

   
    DataTable DataCells = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string EmpLevel;
   
    ReportDocument report = new ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Recruitment Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();
            EmpLevel = Request.QueryString["EmpLevel"].ToString();



            if (EmpLevel == "Trainee")
            {
                SSQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,Convert(varchar,EM.DOJ,103) as DOJ,Sum(Present) as WorkingDays";
                SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID=LD.MachineID and EM.LocCode=LD.LocCode";
                SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                SSQL = SSQL + " And LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
                if (EmpLevel != "")
                {
                    SSQL = SSQL + " And EM.EmpLevel='" + EmpLevel + "'";
                }
                SSQL = SSQL + " Group By EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,EM.DOJ";
            }
            else if (EmpLevel == "SemiSkilled")
            {
                SSQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,Convert(varchar,EM.DOJ,103) as DOJ,TC.Level_Date as LevelDate,Sum(Present) as WorkingDays";
                SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID=LD.MachineID and EM.LocCode=LD.LocCode";
                SSQL = SSQL + " inner join Training_Level_Change TC on EM.MachineID=TC.MachineID and EM.LocCode=TC.Lcode";
                SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                SSQL = SSQL + " And LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TC.Ccode='" + SessionCcode + "' And TC.Lcode='" + SessionLcode + "'";
                if (EmpLevel != "")
                {
                    SSQL = SSQL + " And EM.EmpLevel='" + EmpLevel + "'";
                    SSQL = SSQL + " And TC.Training_Level='" + EmpLevel + "'";
                }
                SSQL = SSQL + " Group By EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,EM.DOJ,TC.Level_Date";
            }
            else if (EmpLevel == "Skilled")
            {
                SSQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,Convert(varchar,EM.DOJ,103) as DOJ,Sum(Present) as WorkingDays";
                SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID=LD.MachineID and EM.LocCode=LD.LocCode";
                SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                SSQL = SSQL + " And LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
                if (EmpLevel != "")
                {
                    SSQL = SSQL + " And EM.EmpLevel='" + EmpLevel + "'";
                }
                SSQL = SSQL + " Group By EM.EmpNo,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,EM.DOJ";
            }
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt.Rows.Count != 0)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();


                ds.Tables.Add(dt);
                //ReportDocument report = new ReportDocument();
                if (EmpLevel == "Trainee")
                {
                    report.Load(Server.MapPath("crystal/TraineeDetailsReport.rpt"));
                }
                else if (EmpLevel == "SemiSkilled")
                {
                    report.Load(Server.MapPath("crystal/SemiSkilledDetailsReport.rpt"));
                }
                else if (EmpLevel == "Skilled")
                {
                    report.Load(Server.MapPath("crystal/SkilledDetailsReport.rpt"));
                }
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
                report.DataDefinition.FormulaFields["LocName"].Text = "'" + SessionLcode.ToString() + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
