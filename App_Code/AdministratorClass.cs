﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Administrator
/// </summary>
public class Administratorclass
{
    private string _Ccode;
    private string _Cname;
    private string _LCode;
    private string _Location;
    private string _Address1;
    private string _Address2;
    private string _District;
    private string _Taluk;
    private string _Pincode;
    private string _State;
    private string _Phone;
    private string _Email;
    private string _Establishment;
	public Administratorclass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Cname
    {
        get { return _Cname; }
        set { _Cname = value; }
    }
    public string Lcode
    {
        get { return _LCode; }
        set { _LCode = value; }
    }
    public string Location
    {
        get { return _Location; }
        set { _Location = value; }
    }
    public string Address1
    {
        get { return _Address1; }
        set { _Address1 = value; }
    }
    public string Address2
    {
        get { return _Address2; }
        set { _Address2 = value; }
    }
    public string District
    {
        get { return _District; }
        set { _District = value; }
    }
    public string Taluk
    {
        get { return _Taluk; }
        set { _Taluk = value; }
    }
    public string Pincode
    {
        get { return _Pincode; }
        set { _Pincode = value; }
    }
    public string state
    {
        get { return _State; }
        set { _State = value; }
    }
    public string Phone
    {
        get { return _Phone; }
        set { _Phone = value; }
    }
    public string Email
    {
        get { return _Email; }
        set { _Email = value; }
    }
    public string Establishment
    {
        get { return _Establishment; }
        set { _Establishment = value; }
    }
}
