﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SalarySlab
/// </summary>
public class SalarySlab
{
    private string _SchmemeName;
    private string _SlabName;
    private string _Amount;
    private string _ID;
    public SalarySlab()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string SchmemeName
    {
        get { return _SchmemeName; }
        set { _SchmemeName = value; }
    }
    public string SlabName
    {
        get { return _SlabName; }
        set { _SlabName = value; }
    }
    public string Amount
    {
        get { return _Amount; }
        set { _Amount = value; }
    }
    public string ID
    {
        get { return _ID; }
        set { _ID = value; }
    }
}
