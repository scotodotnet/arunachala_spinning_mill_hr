﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SalaryClass
/// </summary>
public class SalaryClass
{
    private string _SalaryThrough;
    private string  _FromBankACno;
    private string _FromBankName;
    private string _FromBranch;
    private string _ToBankACNo;
    private string _ToBankName;
    private string _ToBranch;
    private string _BasicAndDA;
    private string _ProvidentFund;
    private string _HRA;
    private string _ESI;
    private string _Conveyance;
    private string _ProvisionalTax;
    private string _CCA;
    private string _IncomTax;
    private string _GrossEarnings;
    private string _SalaryAdvanceTrans;
    private string _MedicalReiburse;
    private string _PerIncentive;
    private string _Allowances1;
    private string _Allowances2;
    private string _Allowances3;
    private string _Allowances4;
    private string _Allowances5;
    private string _Deduction1;
    private string _Deduction2;
    private string _Deduction3;
    private string _Deduction4;
    private string _Deduction5;
    private string _Totalpay;
    private string _TotalDeductions;
    private string _NetPay;
    private string _RoundOffNetPay;
    private string _SalaryDate;
    private string _Cash;
    private string _Hometown;
    private string _FlatRate;
    private string _TotalWorkingdays;
    private string _Availabledays;
    private string _WithPay;
    private string _Withoutpay;
    private string _TotalAmt;
    private string _tobankAcno;
    private string _tobankname;
    private string _tobankbranch;
    private string _deduction1;
    private string _deduction2;
    private string _deduction3;
    private string _deduction4;
    private string _deduction5;
    private string _LossofPay;
    private string _MessDeduction;
    private string _HostelDeduction;
    private string _ApplyLeaveDays;
    private string _LOPDays;
    private string _Ccode;
    private string _Lcode;
    private string _NFh;
    private string _OT;
    private string _FDA;
    private string _VDA;
    private string _Advance;
    private string _stamp;
    private string _Unioncg;
    private string _PfSalary;
    private string _words;
    private string _work;
    private string _weekoff;
    private string _CL;
    private string _FixedBase;
    private string _FixedFDA;
    private string _FixedHRA;
    private string _Emp_PF;
    private string _HalfNightAmt;
    private string _FullNightAmt;
    private string _ThreesidedAmt;
    private string _SpinningAmt;
    private string _DayIncentive;
    private string _BasicforSM;

    //Basic Spilit Declaration
    private string _BasicDA;
    private string _Basic_HRA;
    private string _Conv_Allow;
    private string _Edu_Allow;
    private string _Medi_Allow;
    private string _Basic_RAI;
    private string _Washing_Allow;

    private string _Basic_Spl_Allowance;
    private string _Basic_Uniform;
    private string _Basic_Vehicle_Maintenance;
    private string _Basic_Communication;
    private string _Basic_Journ_Perid_Paper;
    private string _Basic_Incentives;
    private string _Basic_Incentive_Text;

    private string _OTHoursAmt;
    private string _OTHoursNew;

    private string _Leave_Credit_Check;

    private string _Fixed_Work_Days;
    private string _WH_Work_Days;

    private string _Ded_Others1;
    private string _Ded_Others2;

    private string _EmployeerPFone;
    private string _EmployeerPFTwo;
    private string _EmployeerESI;

    private string _Leave_Credit_Days;
    private string _Leave_Credit_Add;
    private string _Leave_Credit_Type;

    public SalaryClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string MessDeduction
    {
        get { return _MessDeduction; }
        set { _MessDeduction = value; }

    }
    public string HostelDeduction
    {
        get { return _HostelDeduction; }
        set { _HostelDeduction = value; }
    }
    
    public string LossofPay
    {
        get { return _LossofPay; }
        set { _LossofPay = value; }
    }
    public string ToBankName
    {
        get { return _tobankname; }
        set { _tobankname = value; }
    }
    public string ToBankBranch
    {
        get { return _tobankbranch; }
        set { _tobankbranch = value; }
    }
    public string Deduction1
    {
        get { return _deduction1; }
        set { _deduction1 = value; }
    }
    public string Deduction2
    {
        get { return _deduction2; }
        set { _deduction2 = value; }
    }
    public string Deduction3
    {
        get { return _deduction3; }
        set { _deduction3 = value; }
    }
    public string Deduction4
    {
        get { return _deduction4; }
        set { _deduction4 = value; }
    }
    public string Deduction5
    {
        get { return _deduction5; }
        set { _deduction5 = value; }
    }
    public string ToBankAccNo
    {
        get { return _tobankAcno; }
        set { _tobankAcno = value; }
    }
    public string SalaryThrough
    {
        get { return _SalaryThrough; }
        set { _SalaryThrough = value; }
    }
    public string  FromBankACno
    {
        get { return _FromBankACno; }
        set { _FromBankACno = value; }
    }
    public string FromBankName
    {
        get { return _FromBankName; }
        set { _FromBankName = value; }
    }
    public string FromBranch
    {
        get { return _FromBranch; }
        set { _FromBranch = value; }
    }
    public string BasicAndDA
    {
        get { return _BasicAndDA; }
        set { _BasicAndDA = value; }
    }
    public string ProvidentFund
    {
        get { return _ProvidentFund; }
        set { _ProvidentFund = value; }
    }
    public string HRA
    {
        get { return _HRA; }
        set { _HRA = value; }
    }
    public string ESI
    {
        get { return _ESI; }
        set { _ESI = value; }
    }
    public string Conveyance
    {
        get { return _Conveyance; }
        set { _Conveyance = value; }
    }
    public string ProvisionalTax
    {
        get { return _ProvisionalTax; }
        set { _ProvisionalTax = value; }
    }
    public string CCA
    {
        get { return _CCA; }
        set { _CCA = value; }
    }
    public string IncomTax
    {
        get { return _IncomTax; }
        set { _IncomTax = value; }
    }
    public string GrossEarnings
    {
        get { return _GrossEarnings; }
        set { _GrossEarnings = value; }
    }
    public string SalaryAdvanceTrans
    {
        get { return _SalaryAdvanceTrans; }
        set { _SalaryAdvanceTrans = value; }
    }
    public string MedicalReiburse
    {
        get { return _MedicalReiburse; }
        set { _MedicalReiburse = value; }
    }
    public string PerIncentive
    {
        get { return _PerIncentive; }
        set { _PerIncentive = value; }
    }
    public string Allowances1
    {
        get { return _Allowances1; }
        set { _Allowances1 = value; }
    }
    public string Allowances2
    {
        get { return _Allowances2; }
        set { _Allowances2 = value; }
    }
    public string Allowances3
    {
        get { return _Allowances3; }
        set { _Allowances3 = value; }
    }
    public string Allowances4
    {
        get { return _Allowances4; }
        set { _Allowances4 = value; }
    }
    public string Allowances5
    {
        get { return _Allowances5; }
        set { _Allowances5 = value; }
    }
    public string Totalpay
    {
        get { return _Totalpay; }
        set { _Totalpay = value; }
    }
    public string TotalDeduction
    {
        get { return _TotalDeductions; }
        set { _TotalDeductions = value; }
    }
    public string NetPay
    {
        get { return _NetPay; }
        set { _NetPay = value; }
    }

    public string RoundOffNetPay
    {
        get { return _RoundOffNetPay; }
        set { _RoundOffNetPay = value; }
    }

    public  string SalaryDate
    {
        get { return _SalaryDate; }
        set { _SalaryDate = value; }
    }
    public string FlateRate
    {
        get { return _FlatRate; }
        set { _FlatRate = value; }
    }
    public string Cash
    {
        get { return _Cash; }
        set { _Cash = value; }
    }
    public string TotalWorkingDays
    {
        get { return _TotalWorkingdays; }
        set { _TotalWorkingdays = value; }
    }
    public string AvailableDays
    {
        get { return _Availabledays; }
        set { _Availabledays = value; }
    }
    public string WithPay
    {
        get { return _WithPay; }
        set { _WithPay = value; }
    }
    public string WithoutPay
    {
        get { return _Withoutpay; }
        set { _Withoutpay = value; }
    }
    public string TotalAmt
    {
        get { return _TotalAmt; }
        set { _TotalAmt = value; }
    }
    public string ApplyLeaveDays
    {
        get { return _ApplyLeaveDays; }
        set { _ApplyLeaveDays = value; }
    }
    public string LOPDays
    {
        get { return _LOPDays; }
        set { _LOPDays = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string NFh
    {
        get { return _NFh; }
        set { _NFh = value; }
    }
    public string OT
    {
        get { return _OT; }
        set { _OT = value; }
    }
    public string FDA
    {
        get { return _FDA; }
        set { _FDA = value; }
    }
    public string VDA
    {
        get { return _VDA; }
        set { _VDA = value; }
    }
    public string Advance
    {
        get { return _Advance; }
        set { _Advance = value; }
    }
    public string Stamp
    {
        get { return _stamp; }
        set { _stamp = value; }
    }
    public string Union
    {
        get { return _Unioncg; }
        set { _Unioncg = value; }
    }
    public string PfSalary
    {
        get { return _PfSalary; }
        set { _PfSalary = value; }
    }
    public string Words
    {
        get { return _words; }
        set { _words = value; }
    }
    public string work
    {
        get { return _work; }
        set { _work = value; }
    }
    public string weekoff
    {
        get { return _weekoff; }
        set { _weekoff = value; }
    }
    public string CL
    {
        get { return _CL; }
        set { _CL = value; }
    }
    public string FixedBase
    {
        get { return _FixedBase; }
        set { _FixedBase = value; }
    }
    public string FixedFDA
    {
        get { return _FixedFDA; }
        set { _FixedFDA = value; }
    }
    public string FixedFRA
    {
        get { return _FixedHRA; }
        set { _FixedHRA = value; }
    }
    public string Emp_PF
    {
        get { return _Emp_PF; }
        set { _Emp_PF = value; }
    }
    public string HomeTown
    {
        get { return _Hometown; }
        set { _Hometown = value; }
    }
    public string HalfNightAmt
    {
        get { return _HalfNightAmt; }
        set { _HalfNightAmt = value; }
    }
    public string FullNightAmt
    {
        get { return _FullNightAmt; }
        set { _FullNightAmt = value; }
    }
    public string ThreesidedAmt
    {
        get { return _ThreesidedAmt; }
        set { _ThreesidedAmt = value; }
    }
    public string SpinningAmt
    {
        get { return _SpinningAmt; }
        set { _SpinningAmt = value; }
    }
    public string DayIncentive
    {
        get { return _DayIncentive; }
        set { _DayIncentive = value; }
    }

    public string BasicforSM
    {
        get { return _BasicforSM; }
        set { _BasicforSM = value; }
    }

    //Basic Spilit Variable Declaration
    public string BasicDA
    {
        get { return _BasicDA; }
        set { _BasicDA = value; }
    }
    public string Basic_HRA
    {
        get { return _Basic_HRA; }
        set { _Basic_HRA = value; }
    }
    public string Conv_Allow
    {
        get { return _Conv_Allow; }
        set { _Conv_Allow = value; }
    }
    public string Edu_Allow
    {
        get { return _Edu_Allow; }
        set { _Edu_Allow = value; }
    }
    public string Medi_Allow
    {
        get { return _Medi_Allow; }
        set { _Medi_Allow = value; }
    }

    public string Basic_RAI
    {
        get { return _Basic_RAI; }
        set { _Basic_RAI = value; }
    }
    public string Washing_Allow
    {
        get { return _Washing_Allow; }
        set { _Washing_Allow = value; }
    }

    public string Basic_Spl_Allowance
    {
        get { return _Basic_Spl_Allowance; }
        set { _Basic_Spl_Allowance = value; }
    }
    public string Basic_Uniform
    {
        get { return _Basic_Uniform; }
        set { _Basic_Uniform = value; }
    }
    public string Basic_Vehicle_Maintenance
    {
        get { return _Basic_Vehicle_Maintenance; }
        set { _Basic_Vehicle_Maintenance = value; }
    }
    public string Basic_Communication
    {
        get { return _Basic_Communication; }
        set { _Basic_Communication = value; }
    }
    public string Basic_Journ_Perid_Paper
    {
        get { return _Basic_Journ_Perid_Paper; }
        set { _Basic_Journ_Perid_Paper = value; }
    }

    public string Basic_Incentives
    {
        get { return _Basic_Incentives; }
        set { _Basic_Incentives = value; }
    }
    public string Basic_Incentive_Text
    {
        get { return _Basic_Incentive_Text; }
        set { _Basic_Incentive_Text = value; }
    }

    public string OTHoursNew
    {
        get { return _OTHoursNew; }
        set { _OTHoursNew = value; }
    }
    public string OTHoursAmt
    {
        get { return _OTHoursAmt; }
        set { _OTHoursAmt = value; }
    }
    public string Leave_Credit_Check
    {
        get { return _Leave_Credit_Check; }
        set { _Leave_Credit_Check = value; }
    }

    public string Fixed_Work_Days
    {
        get { return _Fixed_Work_Days; }
        set { _Fixed_Work_Days = value; }
    }
    public string WH_Work_Days
    {
        get { return _WH_Work_Days; }
        set { _WH_Work_Days = value; }
    }

    public string Ded_Others1
    {
        get { return _Ded_Others1; }
        set { _Ded_Others1 = value; }
    }
    public string Ded_Others2
    {
        get { return _Ded_Others2; }
        set { _Ded_Others2 = value; }
    }

    public string EmployeerPFone
    {
        get { return _EmployeerPFone; }
        set { _EmployeerPFone = value; }
    }
    public string EmployeerPFTwo
    {
        get { return _EmployeerPFTwo; }
        set { _EmployeerPFTwo = value; }
    }
    public string EmployeerESI
    {
        get { return _EmployeerESI; }
        set { _EmployeerESI = value; }
    }


    public string Leave_Credit_Days
    {
        get { return _Leave_Credit_Days; }
        set { _Leave_Credit_Days = value; }
    }
    public string Leave_Credit_Add
    {
        get { return _Leave_Credit_Add; }
        set { _Leave_Credit_Add = value; }
    }
    public string Leave_Credit_Type
    {
        get { return _Leave_Credit_Type; }
        set { _Leave_Credit_Type = value; }
    }
}


