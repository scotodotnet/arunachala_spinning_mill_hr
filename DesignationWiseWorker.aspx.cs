﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class DesignationWiseWorker : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDataTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();


    string Date;
    string Date2;
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string State;
    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- DEsignation Wise Employed Worker";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();

            Date = Request.QueryString["FromDate"].ToString();
            Date2 = Request.QueryString["ToDate"].ToString();
            Division = Request.QueryString["Division"].ToString();

            DataCells.Columns.Add("Department_Name");
            DataCells.Columns.Add("Designation");
            DataCells.Columns.Add("Shift1");
            DataCells.Columns.Add("Shift2");
            DataCells.Columns.Add("Shift3");
            DataCells.Columns.Add("General");
            DataCells.Columns.Add("No_Shift");
            DataCells.Columns.Add("UpToDate");
            DataCells.Columns.Add("Company_Name");
            DataCells.Columns.Add("Location");
            DataCells.Columns.Add("Shift_Count");



            string Ondate_From_Date = "";
            string OnDate_To_Date = "";
            string UpToDate_From_Date = "";
            string UpToDate_To_Date = "";
            Ondate_From_Date = string.Format(Date, "yyyy/MM/dd");
            DateTime date1 = Convert.ToDateTime(Ondate_From_Date);
            OnDate_To_Date = string.Format(Date2, "yyyy/MM/dd");
            DateTime date2 = Convert.ToDateTime(OnDate_To_Date);
            var Todat = date2.AddDays(-1).ToString("yyyy/MM/dd");

            if (Date == Date2)
            {
                UpToDate_From_Date = string.Format(Date2, "yyyy/MM/dd");
                UpToDate_To_Date = string.Format(Date2, "yyyy/MM/dd");
            }
            else
            {
                UpToDate_From_Date = string.Format(Date, "yyyy/MM/dd");
                UpToDate_To_Date = string.Format(Todat, "yyyy/MM/dd");
            }

            SSQL = "Select DeptName,Designation,Count(Present) as Present from Logtime_Days where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "' And Present <> 0.0 ";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103) ";
        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(Date2).ToString("dd/MM/yyyy") + "',103) ";
        SSQL = SSQL + " group by DeptName,Designation";
        SSQL = SSQL + " Order by DeptName,Designation Asc";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);


            DataTable Ondate_DS=new DataTable();
            DataTable UpTodate_DS=new DataTable();

            for(int iRow=0;iRow<mDataSet.Rows.Count ;iRow++)
            {
                string Gen_Count="0";
                string No_Shift="0";
                string Shift1="0";
                string Shift2="0";
                string Shift3="0";
                string UpToDate_Record="0";


                // 'Get On Date Record
            SSQL = "Select isnull([GENERAL],0) as Gen,isnull([No Shift],0) as No_Sh,isnull([SHIFT1],0) S1,";
            SSQL = SSQL + " isnull([SHIFT2],0) as S2,isnull([SHIFT3],0) as S3 from (";
            SSQL = SSQL + " Select Count(Present) as P_Count,Shift from Logtime_Days where CompCode='" + SessionCcode  + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode  + "' And Present <> 0.0";
            SSQL = SSQL + " And Attn_Date_Str >='" + Ondate_From_Date + "' And Attn_Date_Str <='" + OnDate_To_Date + "'";
            SSQL = SSQL + " And DeptName='" + mDataSet.Rows[iRow]["DeptName"] + "'";
            SSQL = SSQL + " And Designation='" + mDataSet.Rows[iRow]["Designation"] + "'";
            SSQL = SSQL + " group by Shift ) as P";
            SSQL = SSQL + " PIVOT(MAX(P_Count) for Shift in ([GENERAL],[No Shift],[SHIFT1],[SHIFT2],[SHIFT3])) as pvt";
            Ondate_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                if(Ondate_DS.Rows.Count != 0)
                {
                      Gen_Count = Ondate_DS.Rows[0]["Gen"].ToString();
                No_Shift = Ondate_DS.Rows[0]["No_Sh"].ToString();
                Shift1 = Ondate_DS.Rows[0]["S1"].ToString();
                Shift2 = Ondate_DS.Rows[0]["S2"].ToString();
                Shift3 = Ondate_DS.Rows[0]["S3"].ToString();
                }
                else
                {
                     Gen_Count = "0";
                No_Shift = "0";
                Shift1 = "0";
                Shift2 = "0";
                Shift3 = "0";
                }


                
            //'Get Upto Date Record
            SSQL = "Select isnull(Count(Present),0) as P_Count from Logtime_Days where CompCode='" +SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode +"' And Present <> 0.0 ";
            SSQL = SSQL + " And Attn_Date >='" + UpToDate_From_Date + "' And Attn_Date <='" + UpToDate_To_Date + "'";
            SSQL = SSQL + " And DeptName='" + mDataSet.Rows[iRow]["DeptName"] + "'";
            SSQL = SSQL + " And Designation='" + mDataSet.Rows[iRow]["Designation"] + "'";
            SSQL = SSQL + " And (Shift='SHIFT1' or Shift='SHIFT2' or Shift='SHIFT3')";
            UpTodate_DS = objdata.RptEmployeeMultipleDetails (SSQL);


                if(UpTodate_DS.Rows.Count !=0)
                {
                    UpToDate_Record = UpTodate_DS.Rows[0]["P_Count"].ToString();
                }
                else
                {
                    UpToDate_Record = "0";
                }
            
                 //'Get Shift Count
               DataTable SH_DS=new DataTable();
                string Shift_Count_Str="0";
          

            SSQL = "Select isnull(sum(Shift_Count),0) as SH_Count from (";
            SSQL = SSQL + " Select Attn_Date, COUNT(distinct Shift) as Shift_Count from Logtime_Days where ";
            SSQL = SSQL + " CompCode='" + SessionCcode  + "' And LocCode='" + SessionLcode  + "' And Present != '0.0'";
            SSQL = SSQL + " And Attn_Date >='" + UpToDate_From_Date + "' And Attn_Date <='" + Ondate_From_Date + "'";
            SSQL = SSQL + " And DeptName='" + mDataSet.Rows[iRow]["DeptName"] + "'";
            SSQL = SSQL + " And Designation='" + mDataSet.Rows[iRow]["Designation"] + "'";
            SSQL = SSQL + " And (Shift='SHIFT1' or Shift='SHIFT2' or Shift='SHIFT3')";
            SSQL = SSQL + " group by Attn_Date ) as p";
            SH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if(SH_DS.Rows.Count !=0)
                {
                     Shift_Count_Str = SH_DS.Rows[0]["SH_Count"].ToString();
                }
                else
                {
                      Shift_Count_Str = "0";
                }

                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();

                 DataCells.NewRow();
                 DataCells.Rows.Add();
                 DataCells.Rows[iRow]["Department_Name"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                 DataCells.Rows[iRow]["Designation"]=mDataSet.Rows[iRow]["Designation"].ToString();
                 DataCells.Rows[iRow]["Shift1"] = Shift1;
                 DataCells.Rows[iRow]["Shift2"] = Shift2;
                 DataCells.Rows[iRow]["Shift3"] = Shift3;
                 DataCells.Rows[iRow]["General"] = Gen_Count;
                 DataCells.Rows[iRow]["No_Shift"] = No_Shift;
                 DataCells.Rows[iRow]["UpToDate"] = UpToDate_Record;
                 DataCells.Rows[iRow]["Company_Name"]=name;
                 DataCells.Rows[iRow]["Location"]=SessionLcode ;
                 DataCells.Rows[iRow]["Shift_Count"] = Shift_Count_Str;


            }

            ds.Tables.Add(DataCells);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Designation_Wise_Worker_Employed.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
           
         

        }

          
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
