﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class CommissonPending_ReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SessionUserType;
    string SSQL;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    string Name;
    string Type;
    DataTable AutoDtable = new DataTable();
    DataTable DT = new DataTable();
    DataTable dt_Name = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Commission Transanction Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Type = Request.QueryString["Type"].ToString();
            Name = Request.QueryString["Name"].ToString();

            Report();
        }
    }

    public void Report()
    {

        AutoDtable.Columns.Add("CompanyName");
        AutoDtable.Columns.Add("LocationName");
        AutoDtable.Columns.Add("ExistingCode");
        AutoDtable.Columns.Add("FirstName");
        AutoDtable.Columns.Add("DOJ");
        AutoDtable.Columns.Add("DOR");
        AutoDtable.Columns.Add("CommAmt");


        //SSQL = "Select *from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And IsActive='No'";
        if (Type == "Agent")
        {
            SSQL = "Select '' as CompanyName,'' as LocationName,EM.ExistingCode,EM.FirstName,Convert(varchar(10),EM.DOJ,103) as DOJ,";
            SSQL = SSQL + "CONVERT(varchar(10),DOR,103) as DOR,";
            SSQL = SSQL + "isnull(Sum(CM.Amount),0) as CommAmt from Employee_Mst EM";
            SSQL = SSQL + " inner join CommisionVoucher_Mst CM on EM.ExistingCode=CM.TokenNo";
            SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='No'";
            SSQL = SSQL + " And CM.CompCode='" + SessionCcode + "' And CM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.ReferalType='" + Type + "' And CM.ReferalType='" + Type + "'";
            if (Name != "-Select-")
            {
                SSQL = SSQL + " And EM.AgentName='" + Name + "'";
                SSQL = SSQL + " And CM.ReferalName='" + Name + "'";
            }
            SSQL = SSQL + " And DATEDIFF(DD,CONVERT(datetime,EM.DOJ,103),Convert(datetime,EM.DOR,103))>90";
            SSQL = SSQL + " Group by EM.ExistingCode,EM.FirstName,EM.DOJ,EM.DOR";
        }
        if (Type == "Parent")
        {
            SSQL = "Select '' as CompanyName,'' as LocationName,EM.ExistingCode,EM.FirstName,Convert(varchar(10),EM.DOJ,103) as DOJ,";
            SSQL = SSQL + "CONVERT(varchar(10),DOR,103) as DOR,";
            SSQL = SSQL + "isnull(Sum(CM.Amount),0) as CommAmt from Employee_Mst EM";
            SSQL = SSQL + " inner join CommisionVoucher_Mst CM on EM.ExistingCode=CM.TokenNo";
            SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='No'";
            SSQL = SSQL + " And CM.CompCode='" + SessionCcode + "' And CM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.ReferalType='" + Type + "' And CM.ReferalType='" + Type + "'";
            if (Name != "-Select-")
            {
                SSQL = SSQL + " And EM.RefParentsName='" + Name + "'";
                SSQL = SSQL + " And CM.ReferalName='" + Name + "'";
            }
            SSQL = SSQL + " And DATEDIFF(DD,CONVERT(datetime,EM.DOJ,103),Convert(datetime,EM.DOR,103))>90";
            SSQL = SSQL + " Group by EM.ExistingCode,EM.FirstName,EM.DOJ,EM.DOR";
        }
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt_Name = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt_Name.Rows[0]["CompName"].ToString();
            for (int i = 0; i < DT.Rows.Count; i++)
            {

                DataTable DT_Recv = new DataTable();

                SSQL = "Select isnull(Sum(Amount),0) as RecvAmt from CommisionRecovery_Mst Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And TokenNo='" + DT.Rows[i]["ExistingCode"].ToString() + "'";
                DT_Recv = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT_Recv.Rows.Count != 0)
                {
                    if (Convert.ToDecimal(DT.Rows[i]["CommAmt"].ToString()) > Convert.ToDecimal(DT_Recv.Rows[0]["RecvAmt"].ToString()))
                    {
                        string RecvAmt = (Convert.ToDecimal(DT.Rows[i]["CommAmt"].ToString()) - Convert.ToDecimal(DT_Recv.Rows[0]["RecvAmt"].ToString())).ToString();

                        AutoDtable.NewRow();
                        AutoDtable.Rows.Add();

                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["ExistingCode"] = DT.Rows[i]["ExistingCode"].ToString();
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["FirstName"] = DT.Rows[i]["FirstName"].ToString();
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DOJ"] = DT.Rows[i]["DOJ"].ToString();
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DOR"] = DT.Rows[i]["DOR"].ToString();
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CommAmt"] = RecvAmt.ToString();
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["LocationName"] = SessionLcode;
                        AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CompanyName"] = name;

                    }
                }
                else
                {
                    AutoDtable.NewRow();
                    AutoDtable.Rows.Add();

                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["ExistingCode"] = DT.Rows[i]["ExistingCode"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["FirstName"] = DT.Rows[i]["FirstName"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DOJ"] = DT.Rows[i]["DOJ"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DOR"] = DT.Rows[i]["DOR"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CommAmt"] = DT.Rows[i]["CommAmt"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["LocationName"] = SessionLcode;
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CompanyName"] = name;
                }
            }

            if (AutoDtable.Rows.Count != 0)
            {
                ds.Tables.Add(AutoDtable);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/CommissionRecoveryPending.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
