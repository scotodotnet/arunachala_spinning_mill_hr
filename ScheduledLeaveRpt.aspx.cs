﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class ScheduledLeaveRpt : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SSQL = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    DataTable DT = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Scheduled Leave Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();


            SSQL = "Select convert(varchar,EmpNo) as EmpNo,ExistingCode,FirstName,DeptName,Designation,";
            SSQL = SSQL + "(left(DATENAME(DD, Convert(datetime,LeaveFrom,103)),2))+'-'+(left(DATENAME(MM, Convert(datetime,LeaveFrom,103)),3)) AS LeaveFrom1,";
            SSQL = SSQL + "(left(DATENAME(DD, Convert(datetime,LeaveTo,103)),2))+'-'+(left(DATENAME(MM, Convert(datetime,LeaveTo,103)),3)) AS LeaveTo1,";
            SSQL = SSQL + "Festival1,Days1,";
            SSQL = SSQL + "(left(DATENAME(DD, Convert(datetime,LeaveFrom2,103)),2))+'-'+(left(DATENAME(MM, Convert(datetime,LeaveFrom2,103)),3)) AS LeaveFrom2,";
            SSQL = SSQL + "(left(DATENAME(DD, Convert(datetime,LeaveTo2,103)),2))+'-'+(left(DATENAME(MM, Convert(datetime,LeaveTo2,103)),3)) AS LeaveTo2,";
            SSQL = SSQL + "Festival2,Days2";
            SSQL = SSQL + " from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
            SSQL = SSQL + " And (LeaveFrom!='' And LeaveTo!='' or LeaveFrom2!='' and LeaveTo2!='')";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();


                ds.Tables.Add(DT);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/ScheduledLeaveReport.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.DataDefinition.FormulaFields["CompName"].Text = "'" + name.ToString() + "'";
                report.DataDefinition.FormulaFields["LocCode"].Text = "'" + SessionLcode.ToString() + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
