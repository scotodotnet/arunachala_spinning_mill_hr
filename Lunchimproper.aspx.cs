﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class Lunchimproper : System.Web.UI.Page
{

   string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    string Datestr = "";
    string Datestr1 = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    string Division = "";
    string SSQL = "";
    string logtimein = "";
    string shift_Check = "";
    string endtime = "";
    string machineidEncrypt = "";
    string MachineID = "";
    DataTable DataCell = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable dttime = new DataTable();
    DataTable mLocalDS1 = new DataTable();
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

   
    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Lunch improper Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
           
          
                GetAttdDayWise_ImproperLunch();
            }
        }
    public void GetAttdDayWise_ImproperLunch()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("TimeIN");

        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");

        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        DataTable mLocalDS = new DataTable();

        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(ng);
        DateTime date2 = date1.AddDays(1);
        

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
            SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
            SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";

            SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";

            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            if (ShiftType1 != "ALL")
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
            if (Date != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
            }

            SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
            AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


            if (AutoDTable.Rows.Count != 0)
            {
                int i = 0;
                for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
                {
                    shift_Check = "";
                    shift_Check = AutoDTable.Rows[iRow]["Shift"].ToString();
                    MachineID = AutoDTable.Rows[iRow]["MachineID"].ToString();
                    machineidEncrypt = UTF8Encryption(AutoDTable.Rows[iRow]["MachineID"].ToString());


                    SSQL = "";
                    SSQL = "Select MachineID,TimeOUT from LogTimeLunch_OUT";
                    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                    SSQL = SSQL + "And TimeOUT >'" + date1.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                    SSQL = SSQL + "And TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "05:00" + "' ";
                    SSQL = SSQL + " and MachineID NOT IN (Select MachineID from LogTimeLunch_IN ";
                    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                    SSQL = SSQL + "And TimeIN >'" + date1.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                    SSQL = SSQL + "And TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "05:00" + "') ";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    string name = dt.Rows[0]["CompName"].ToString();
                    if (mLocalDS.Rows.Count != 0)
                    {
                        for (int k = 0; k < mLocalDS.Rows.Count; k++)
                        {
                            DataCell.NewRow();
                            DataCell.Rows.Add();

                            DataCell.Rows[DataCell.Rows.Count - 1]["SNo"] = i + 1;
                            DataCell.Rows[DataCell.Rows.Count - 1]["TimeOUT"] = String.Format("{0:hh:mm tt}", mLocalDS.Rows[0]["TimeOUT"]);
                            DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = shift_Check;
                            DataCell.Rows[DataCell.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[iRow]["MachineID"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[iRow]["MachineID"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Dept"] = AutoDTable.Rows[iRow]["DeptName"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Type"] = AutoDTable.Rows[iRow]["TypeName"].ToString();

                            DataCell.Rows[DataCell.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[iRow]["ExistingCode"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Name"] = AutoDTable.Rows[iRow]["FirstName"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["ShiftDate"] = Date;
                            DataCell.Rows[DataCell.Rows.Count - 1]["CompanyName"] = name;
                            DataCell.Rows[DataCell.Rows.Count - 1]["LocationName"] = SessionLcode;
                        }
                    }
                    SSQL = "";
                    SSQL = "Select MachineID,TimeIN from LogTimeLunch_IN";
                    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                    SSQL = SSQL + "And TimeIN >'" + date1.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                    SSQL = SSQL + "And TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "05:00" + "' ";
                    SSQL = SSQL + " and MachineID NOT IN (Select MachineID from LogTimeLunch_OUT ";
                    SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                    SSQL = SSQL + "And TimeOUT >'" + date1.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                    SSQL = SSQL + "And TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "05:00" + "') ";
                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);


                    if (mLocalDS1.Rows.Count != 0)
                    {
                        for (int k = 0; k < mLocalDS1.Rows.Count; k++)
                        {
                            DataCell.NewRow();
                            DataCell.Rows.Add();

                            DataCell.Rows[DataCell.Rows.Count - 1]["SNo"] = i + 1;
                            DataCell.Rows[DataCell.Rows.Count - 1]["TimeIN"] = String.Format("{0:hh:mm tt}", mLocalDS1.Rows[0]["TimeIN"]);
                            DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = shift_Check;
                            DataCell.Rows[DataCell.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[iRow]["MachineID"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[iRow]["MachineID"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Dept"] = AutoDTable.Rows[iRow]["DeptName"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Type"] = AutoDTable.Rows[iRow]["TypeName"].ToString();

                            DataCell.Rows[DataCell.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[iRow]["ExistingCode"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["Name"] = AutoDTable.Rows[iRow]["FirstName"].ToString();
                            DataCell.Rows[DataCell.Rows.Count - 1]["ShiftDate"] = Date;
                            DataCell.Rows[DataCell.Rows.Count - 1]["CompanyName"] = name;
                            DataCell.Rows[DataCell.Rows.Count - 1]["LocationName"] = SessionLcode;
                        }
                    }
                }
                ds.Tables.Add(DataCell);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Lunchimproper.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }


    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
