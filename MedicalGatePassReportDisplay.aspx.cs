﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class MedicalGatePassReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SSQL;
    DataTable AutoDTable = new DataTable();
    DataSet ds = new DataSet();
    string Sessionyear;
    string SessionMonth;
    string FromDate;
    string ToDate;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string TransNo;
   
   
    DataRow dtRow;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Medical Gate Pass Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

             SessionCcode = Session["Ccode"].ToString();
             SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();

            TransNo = Request.QueryString["TransNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Report();
        }
    }
    public void Report()
    {
        AutoDTable.Columns.Add("SNO");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("TransNo");
        AutoDTable.Columns.Add("Date");
        AutoDTable.Columns.Add("TokenID");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Reason");
        AutoDTable.Columns.Add("Vehicle_No");

        SSQL="Select * from Medical_GatePass where Ccode='"+SessionCcode +"' and Lcode='"+SessionLcode +"' ";
        if (TransNo != "-Select-")
        {
            SSQL = SSQL + " and TransID='" + TransNo .ToString()+ "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";

        }
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        if (mDataSet.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                string name = dt.Rows[0]["CompName"].ToString();

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();



                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNO"] = sno;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CompanyName"] = name;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LocationName"] = SessionLcode;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TransNo"] = mDataSet.Rows[i]["TransID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Date"] = mDataSet.Rows[i]["TransDate"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TokenID"] = mDataSet.Rows[i]["TokenID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = mDataSet.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Reason"] = mDataSet.Rows[i]["Reason"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Vehicle_No"] = mDataSet.Rows[i]["Vehicle_No"].ToString();

                sno = sno + 1;

            }
            if (AutoDTable.Rows.Count != 0)
            {
                ds.Tables.Add(AutoDTable);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/MedicalGatePass.Rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
