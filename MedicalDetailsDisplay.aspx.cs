﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class MedicalDetailsDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SSQL;
    DataTable AutoDTable = new DataTable();
    DataSet ds = new DataSet();
    string Sessionyear;
    string SessionMonth;
    string FromDate;
    string ToDate;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string RptName;


    DataRow dtRow;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Medical Gate Pass Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();

            RptName = Request.QueryString["RptName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            if (RptName == "Details")
            {
                Details();
            }
            if (RptName == "Abstract")
            {
                Abstract();
            }
            if (RptName == "Consolidate")
            {
                Consolidate();
            }
        }
    }
    public void Abstract()
    {
        AutoDTable.Columns.Add("SNo");
         AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Date");
        AutoDTable.Columns.Add("NoOFEMp");
        AutoDTable.Columns.Add("Amount");


        SSQL = "Select TransDate,Count(MachineID)as MachineID,Sum(Amount)as Amount from MedicalDetails";
        SSQL = SSQL + " Where Ccode='"+SessionCcode +"' and Lcode='"+SessionLcode +"'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";

        }
        SSQL = SSQL + "   group by TransDate ";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                string name = dt.Rows[0]["CompName"].ToString();

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();



                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNO"] = sno;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CompanyName"] = name;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LocationName"] = SessionLcode;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Date"] = mDataSet.Rows[i]["TransDate"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NoOFEMp"] = mDataSet.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = mDataSet.Rows[i]["Amount"].ToString();


                sno = sno + 1;

            }
               ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/MedicalAbstract.Rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }


    public void Consolidate()
    {

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
 
        AutoDTable.Columns.Add("NoOFEMp");
        AutoDTable.Columns.Add("Amount");


        SSQL = "Select Lcode,Count(MachineID)as MachineID,Sum(Amount)as Amount from MedicalDetails";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' ";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";

        }
        SSQL = SSQL + "   group by Lcode ";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                string name = dt.Rows[0]["CompName"].ToString();

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();



                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNO"] = sno;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CompanyName"] = name;

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LocationName"] = mDataSet.Rows[i]["Lcode"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NoOFEMp"] = mDataSet.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = mDataSet.Rows[i]["Amount"].ToString();


                sno = sno + 1;

            }
            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/MedicalConsolidate.Rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
    public void Details()
    {
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("TokenID");
        AutoDTable.Columns.Add("Date");
        AutoDTable.Columns.Add("Reason");
        AutoDTable.Columns.Add("MedicalName");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("Remarks");


        SSQL = "Select * from MedicalDetails";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "' ";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " and  CONVERT(DATETIME,TransDate,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";

        }
     
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                string name = dt.Rows[0]["CompName"].ToString();

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();


                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNO"] = sno;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CompanyName"] = name;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LocationName"] = SessionLcode ;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TokenID"] = mDataSet.Rows[i]["TokenID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Date"] = mDataSet.Rows[i]["TransDate"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Reason"] = mDataSet.Rows[i]["Reason"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MedicalName"] = mDataSet.Rows[i]["MedicalDetails"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = mDataSet.Rows[i]["Amount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Remarks"] = mDataSet.Rows[i]["Remarks"].ToString();

                sno = sno + 1;

            }
            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/MedicalDetails.Rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
