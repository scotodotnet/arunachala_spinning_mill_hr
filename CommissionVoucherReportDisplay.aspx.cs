﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class CommissionVoucherReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
      
            string SessionUserType;
    string SSQL;
    string FromDate;
    string ToDate;
    string TransID;
    DataTable AutoDTable = new DataTable();
    static bool isUK = false;
    static string Words = "";
    string RoundOffNetPay = "";
    string NetPay = "";
    DataTable DT = new DataTable();
    DataTable dt_Name = new DataTable();
    DateTime frmDate;
    DateTime toDate;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Commission Voucher Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

              SessionCcode = Session["Ccode"].ToString();
             SessionLcode = Session["Lcode"].ToString();
           SessionUserType = Session["Isadmin"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            TransID = Request.QueryString["TransID"].ToString();
            Report();
        }
            
    }
    public void Report()
    {
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("TransID");
        AutoDTable.Columns.Add("TransDate");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("DOJ");
        AutoDTable.Columns.Add("RefferalName");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("Note");
        AutoDTable.Columns.Add("AmountInWords");

        AutoDTable.Columns.Add("Commission_Full_Amt");
        AutoDTable.Columns.Add("Commission_Paid_Amt");

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

       SSQL=" Select CVM.TransID,CVM.TransDate,CVM.TokenNo,EM.FirstName,Em.DOJ,CVM.Amount,";
       SSQL=SSQL +"CVM.Note,CVM.ReferalName from CommisionVoucher_Mst CVM inner join";
       SSQL = SSQL + " Employee_Mst EM on EM.ExistingCode=CVM.TokenNo And EM.CompCode=CVM.CompCode And EM.LocCode=CVM.LocCode where CVM.CompCode='" + SessionCcode + "'";
        SSQL=SSQL+ "and CVM.LocCode='"+SessionLcode +"' and EM.CompCode='"+SessionCcode +"'and EM.LocCode='"+SessionLcode +"'";
        if (TransID != "-Select-")
        {
            SSQL = SSQL + " and CVM.TransID='" + TransID + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,CVM.TransDate, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + "  And CONVERT(DATETIME,CVM.TransDate, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt_Name = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt_Name.Rows[0]["CompName"].ToString();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                NetPay = DT.Rows[i]["Amount"].ToString();
                double d1 = Convert.ToDouble(NetPay.ToString());
              //  int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                RoundOffNetPay = d1.ToString();
                Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";
                string DOB_Convert_Format = Convert.ToDateTime(DT.Rows[i]["DOJ"]).ToString("dd/MM/yyyy");

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TransID"] = DT.Rows[i]["TransID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TransDate"] = DT.Rows[i]["TransDate"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TokenNo"] = DT.Rows[i]["TokenNo"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = DT.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DOJ"] = DOB_Convert_Format;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = DT.Rows[i]["Amount"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Note"] = DT.Rows[i]["Note"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["RefferalName"] = DT.Rows[i]["ReferalName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LocationName"] = SessionLcode;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["CompanyName"] = name;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AmountInWords"] ="("+ Words + ")";

                //Get Commission Balance Details...
                string Commission_Full_Amt = "0";
                string Commission_Paid_Amt = "0";
                string Commission_Balance_Amt = "0";
                DataTable DT_Co = new DataTable();
                SSQL = "Select isnull(sum(cast(Credit as decimal(18,2))),0) as CR,isnull(sum(cast(Debit as decimal(18,2))),0) as DR";
                SSQL = SSQL + " from Commission_Transaction_Ledger where Token_No='" + DT.Rows[i]["TokenNo"].ToString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DT_Co = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Co.Rows.Count != 0)
                {
                    Commission_Full_Amt = DT_Co.Rows[0]["CR"].ToString();
                    Commission_Paid_Amt = DT_Co.Rows[0]["DR"].ToString();
                }
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Commission_Full_Amt"] = Commission_Full_Amt;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Commission_Paid_Amt"] = Commission_Paid_Amt;
            }

            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Commision_Voucher.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
   
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
