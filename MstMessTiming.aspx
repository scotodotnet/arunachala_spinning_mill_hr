﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstMessTiming.aspx.cs" Inherits="MstMessTiming" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Mess Timing</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Mess Timing </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Mess Timing</h4>
                        </div>
                        <div class="panel-body">
                      <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label> Type</label>
								  <asp:DropDownList runat="server" ID="txtType" class="form-control select2">
								  <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								  <asp:ListItem Value="BreakFast">BreakFast</asp:ListItem>
								  <asp:ListItem Value="Lunch">Lunch</asp:ListItem>
								  <asp:ListItem Value="Dinner">Dinner</asp:ListItem>
								  </asp:DropDownList>
							   </div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Start Time</label>
								  <asp:TextBox runat="server" ID="txtStartTime" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>End Time</label>
								  <asp:TextBox runat="server" ID="txtEndTime" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                      <!-- end row -->
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

