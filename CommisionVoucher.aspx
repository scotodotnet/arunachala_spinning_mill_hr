﻿<%@ Page Language="C#"  MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="CommisionVoucher.aspx.cs" Inherits="CommisionVoucher" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Commission</a></li>
				<li class="active">Commission Voucher</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Commission Voucher</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Commission Voucher</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>TransID</label>
								<asp:TextBox runat="server" ID="txtTransid" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtTransid" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                            <div class="form-group">
                           <label>TransDate</label>
                          	<asp:TextBox runat="server" ID="txtTransDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtTransDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                       
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtTransDate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                          </div>
                         </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								 <asp:DropDownList runat="server" ID="ddlTokenNo" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlTokenNo_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
								</div>
                             </div>
                           <!-- end col-4 -->
                          
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Refferal Type</label>
								<asp:TextBox runat="server" ID="txtrefferalType" class="form-control"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Refferal Name</label>
								<asp:TextBox runat="server" ID="txtrefferalName" class="form-control"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Address</label>
								<asp:TextBox runat="server" ID="txtAddress" class="form-control" TextMode="MultiLine"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                         <!-- begin row -->
                         <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Commision Amount</label>
								    <asp:Label runat="server" ID="txtCommission_Tot_Amt" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Paid Amount</label>
								    <asp:Label runat="server" ID="txtCommission_Paid_Amt" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Balance Amount</label>
								    <asp:Label runat="server" ID="txtCommission_Balance_Amt" class="form-control" BackColor="LightGray"></asp:Label>
								</div>
                            </div>
                         </div>
                          <div class="row">
                             <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Payment Mode</label>
								 <asp:DropDownList runat="server" ID="ddlPaymentMode" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlPaymentMode_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Cash</asp:ListItem>
                                        <asp:ListItem Value="1">Check</asp:ListItem> 
                                        <asp:ListItem Value="2">Neft</asp:ListItem>
							 	 </asp:DropDownList>
							 	
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Check No</label>
										<asp:TextBox runat="server" ID="txtCheckNo" class="form-control" Enabled="false" ></asp:TextBox>
										
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Check Date</label>
										<asp:TextBox runat="server" ID="txtCheckDate" class="form-control datepicker" placeholder="dd/MM/YYYY" Enabled="false" ></asp:TextBox>
									</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Amount</label>
										<asp:TextBox runat="server" ID="txtAmount" class="form-control" Text="0"></asp:TextBox>
										</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Note</label>
										<asp:TextBox runat="server" ID="txtNote" class="form-control" TextMode="MultiLine"></asp:TextBox>
									</div>
                                  </div>
                                
                          </div>
                       <!-- end row -->  
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                         
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                      
                 
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
                                         	<asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-warning" 
                                         onclick="btnBack_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->   
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

