﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class CommunitywiseEmployeeCount : System.Web.UI.Page
{

    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    string Division = "";
    string FromDate;
    string ToDate;
    string SSQL = "";

    DataTable DataCell = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable dt_table = new DataTable();
    DataTable dt = new DataTable();
    DataSet ds = new DataSet();
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-CommnityWise Employee CountReport";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();



            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Division = Request.QueryString["Division"].ToString();


            EmployeeCount();
        }
    }
    public void EmployeeCount()
    {
    
        SSQL = "Select '' as CompanyName,'' as LocationName,DeptName,ISNULL(Male,0)as Male,ISNULL(Female,0) as Female,(ISNULL(Male,0) + ISNULL(Female,0))as Total,'' as SC,'' as ST ,'' as Minority";
        SSQL = SSQL + " from(select DeptName,COUNT(EmpNo)as EmpNo,Gender from Employee_Mst where IsActive='yes'and LocCode='"+SessionLcode +"' and CompCode='"+ SessionCcode +"'";
        SSQL = SSQL + " And CONVERT(DATETIME,DOJ,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,DOJ,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " group by DeptName,Gender)as s pivot (Sum(EmpNo) for [Gender] in ([Male],[Female] ))as Pivot_tbl order by DeptName";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        if (AutoDTable.Rows.Count != 0)
        {

            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {

         SSQL = "Select DeptName,ISNULL(SC,0)as SC,ISNULL(ST,0) as ST from(select DeptName,";
         SSQL=SSQL + " COUNT(EmpNo)as EmpNo,Community  from Employee_Mst where IsActive='yes'and ";
         SSQL=SSQL + " LocCode='"+SessionLcode +"' and CompCode='"+ SessionCcode +"' and DeptName='"+AutoDTable.Rows[i]["DeptName"].ToString()+"'";
         SSQL = SSQL + " And CONVERT(DATETIME,DOJ,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
         SSQL = SSQL + " And CONVERT(DATETIME,DOJ,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
         SSQL = SSQL + " group by DeptName,Community)as s pivot (Sum(EmpNo) for [Community] in ([SC],[ST] ))as  Pivot_tbl order by DeptName ";
         dt_table = objdata.RptEmployeeMultipleDetails(SSQL);


         if (dt_table.Rows.Count != 0)
         {
             AutoDTable.Rows[i]["SC"] = dt_table.Rows[0]["SC"].ToString();
             AutoDTable.Rows[i]["ST"] = dt_table.Rows[0]["ST"].ToString();


             int totalval=Convert.ToInt32( AutoDTable.Rows[i]["Total"].ToString());
             int sctotal=Convert.ToInt32( AutoDTable.Rows[i]["SC"].ToString());
             int sttotal=Convert.ToInt32( AutoDTable.Rows[i]["ST"].ToString());

             int minority=totalval-(sctotal+sttotal);

              AutoDTable.Rows[i]["Minority"] = minority;

              SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
              dt = objdata.RptEmployeeMultipleDetails(SSQL);
              string name = dt.Rows[0]["CompName"].ToString();

              AutoDTable.Rows[i]["CompanyName"] = name;
              AutoDTable.Rows[i]["LocationName"] = SessionLcode;

         }

            
            }

            ds.Tables.Add(AutoDTable);
                //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Communitywise.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
        }
             else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }


           

        
    }

}
