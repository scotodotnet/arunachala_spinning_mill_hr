﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class CommissonTransactionLedger_ReportDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SessionUserType;
    string SSQL;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    string Name;
    string Type;
    DataTable AutoDtable = new DataTable();
    DataTable DT = new DataTable();
    DataTable dt_Name = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Commission Transanction Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Type = Request.QueryString["Type"].ToString();
            Name = Request.QueryString["Name"].ToString();
          
            Report();
        }
    }
    public void Report()
    {
        if (Type != "-Select-" && Name == "-Select-")
        {
            AutoDtable.Columns.Add("CompanyName");
            AutoDtable.Columns.Add("LocationName");
            AutoDtable.Columns.Add("SNo");
            AutoDtable.Columns.Add("AgentName");
            AutoDtable.Columns.Add("MobileNo");
            AutoDtable.Columns.Add("Credit");
            AutoDtable.Columns.Add("Debit");

            if (Type == "Agent")
            {
                SSQL = "Select CTL.ReferalName ,MA.Mobile,SUM(CTL.Credit)as Credit,SUM(CTL.Debit)as Debit";
                SSQL = SSQL + " from Commission_Transaction_Ledger CTL inner join MstAgent MA on MA.AgentName=CTL.ReferalName  ";
                SSQL = SSQL + " where CTL.ReferalType='" + Type + "' and CTL.CompCode='" + SessionCcode + "' and CTL.LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " group by CTL.ReferalName ,MA.Mobile";
            }

            if (Type == "Parent")
            {
                SSQL = "Select CTL.ReferalName ,Em.RefParentsMobile as Mobile,SUM(CTL.Credit)as Credit,SUM(CTL.Debit)as Debit ";
                SSQL = SSQL + " from Commission_Transaction_Ledger CTL inner join Employee_MSt Em on Em.RefParentsName=CTL.ReferalName  ";
                SSQL = SSQL + " where CTL.ReferalType='" + Type + "' and CTL.CompCode='" + SessionCcode + "' and CTL.LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " group by CTL.ReferalName ,Em.RefParentsMobile";
            }
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt_Name = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt_Name.Rows[0]["CompName"].ToString();
                int sno = 1;
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    AutoDtable.NewRow();
                    AutoDtable.Rows.Add();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["SNo"] = sno;
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["AgentName"] = DT.Rows[i]["ReferalName"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["MobileNo"] = DT.Rows[i]["Mobile"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Credit"] = DT.Rows[i]["Credit"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Debit"] = DT.Rows[i]["Debit"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["LocationName"] = SessionLcode;
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CompanyName"] = name;

                    sno = sno + 1;
                }


                ds.Tables.Add(AutoDtable);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/CommisionLedgerofType.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
        else
        {
            AutoDtable.Columns.Add("CompanyName");
            AutoDtable.Columns.Add("LocationName");
            AutoDtable.Columns.Add("AgentName");
            AutoDtable.Columns.Add("TokenNo");
            AutoDtable.Columns.Add("Name");
            AutoDtable.Columns.Add("DeptName");
            AutoDtable.Columns.Add("DOJ");
            AutoDtable.Columns.Add("Scheme");
            AutoDtable.Columns.Add("Credit");
            AutoDtable.Columns.Add("Debit");
            AutoDtable.Columns.Add("Balance");

            SSQL= "  Select CTL.Token_No,EM.FirstName,EM.DeptName,EM.DOJ,EM.HostelExp,SUM(CTL.Credit)as Credit,SUM(CTL.Debit)as Debit";
            SSQL =SSQL + " from Commission_Transaction_Ledger CTL inner join Employee_Mst EM on EM.EmpNo =CTL.Token_No ";
            SSQL =SSQL + " where CTL.ReferalType='"+Type +"' and CTL.ReferalName='"+Name+"'";
            SSQL =SSQL + " and EM.CompCode='" + SessionCcode + "' and Em.LocCode='" + SessionLcode + "'and CTL.CompCode='" + SessionCcode + "' and CTL.LocCode='" + SessionLcode + "'";
            SSQL =SSQL + " group by CTL.Token_No ,EM.FirstName,EM.DeptName,EM.DOJ,EM.HostelExp";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt_Name = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt_Name.Rows[0]["CompName"].ToString();
                int sno = 1;
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    AutoDtable.NewRow();
                    AutoDtable.Rows.Add();
                 
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["TokenNo"] = DT.Rows[i]["Token_No"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Name"] = DT.Rows[i]["FirstName"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DeptName"] = DT.Rows[i]["DeptName"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["DOJ"] = Convert.ToDateTime(DT.Rows[i]["DOJ"]).ToString("dd/MM/yyyy");
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Scheme"] = DT.Rows[i]["HostelExp"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Credit"] = DT.Rows[i]["Credit"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["Debit"] = DT.Rows[i]["Debit"].ToString();
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["AgentName"] = Name;
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["LocationName"] = SessionLcode;
                    AutoDtable.Rows[AutoDtable.Rows.Count - 1]["CompanyName"] = name;

                    sno = sno + 1;
                }
                
                    
                ds.Tables.Add(AutoDtable);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/CommisionLedgerName.rpt"));
                
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {

                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
