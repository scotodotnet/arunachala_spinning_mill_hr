﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.IO;
using System.Text;

public partial class PayrollMaster : System.Web.UI.MasterPage
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
  
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUser;
    string SessionRights;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUser = Session["Usernmdisplay"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        lblComany.Text = SessionCcode + " - " + SessionLcode;
        lblusername.Text = Session["Usernmdisplay"].ToString();
        lblUserID.Text = Session["Usernmdisplay"].ToString();
        
        if (!IsPostBack)
        {
            //string SQL = "select ModuleLink from [Arunachalam_Rights]..Module_List where ModuleID='6'";
            //DataTable dt = new DataTable();
            //dt = objdata.RptEmployeeMultipleDetails(SQL);
            //if (dt.Rows.Count > 0)
            //{
            //    MainPage_Link.Attributes["href"] = dt.Rows[0]["ModuleLink"].ToString();
            //}
            //else
            {
                MainPage_Link.Attributes["href"] = "/Default.aspx";
            }
            if (SessionUser == "Scoto")
            {

            }
            else if (SessionAdmin != "1")
            {
                //ALL_Menu_Header_Forms_Disable();
                ////Admin_ModuleLink_Check();
                //Admin_User_Rights_Check();
                NonAdmin_User_Rights_Check();

                string IF_Query = "";
                DataTable IF_DT = new DataTable();
                IF_Query = "Select * from [" + SessionRights + "]..Company_Module_User_Rights where ModuleName='Attendance'";
                IF_Query = IF_Query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And UserName='" + SessionUser + "'";
                IF_Query = IF_Query + " And (AddRights='1')";
                IF_DT = objdata.RptEmployeeMultipleDetails(IF_Query);
                if (IF_DT.Rows.Count == 0)
                {
                    Mnu_BioMetric_Link_ID.Visible = false;
                }
            }
            if (SessionAdmin == "1")
            {
                // ALL_Menu_MDHeader_Forms_Disable();
                Admin_User_Rights_Check();
            }
        }
    }

    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Menu_PayDashboard").Visible = false;
        this.FindControl("Menu_PayMaster").Visible = false;
        this.FindControl("Menu_PaySalProc").Visible = false;
        this.FindControl("Menu_PayMstAdvance").Visible = false;
        this.FindControl("Menu_PayBonusMst").Visible = false;
        this.FindControl("Menu_PayReport").Visible = false;        
    }

    private void ALL_Menu_Header_Forms_Disable()
    {
        //Dashboard
        this.FindControl("Menu_PayDashboard").Visible = false;

        //Master
        this.FindControl("Menu_PayMstPF").Visible = false;
        this.FindControl("Menu_PayMstIncentive").Visible = false;
        this.FindControl("Manu_PayMstBasicDet").Visible = false;
        this.FindControl("Menu_PayMstBonus").Visible = false;
        this.FindControl("Menu_PayMstBasicIncText").Visible = false;
        this.FindControl("Menu_PayMstEmpTransfer").Visible = false;
        
        
        //Salary Process
        this.FindControl("Menu_PaySalCalc").Visible = false;
        this.FindControl("Menu_Attendance_Upload").Visible = false;
        this.FindControl("Menu_PayWagesUpload").Visible = false;
        this.FindControl("Menu_SalaryProcessRemoval").Visible = false;


        //Advance
        this.FindControl("Menu_PayAdvance").Visible = false;
        this.FindControl("Menu_RePayAdvance").Visible = false;
        this.FindControl("Menu_PayAdvanceReport").Visible = false;
        

        //Bonus
        this.FindControl("Menu_PayBonusProc").Visible = false;

        //Report        
        this.FindControl("MnuRptPayEmpDet").Visible = false;
        this.FindControl("MnuRptPaySalHis").Visible = false;
        this.FindControl("MnuRptPayELCL").Visible = false;
        this.FindControl("MnuRptPayAttendance").Visible = false;
        this.FindControl("MnuRptPayPFESI").Visible = false;
        this.FindControl("MnuRptPayPFESI_Opt").Visible = false;
        this.FindControl("MnuRptPaySlip").Visible = false;
        this.FindControl("MnuRptPaySlip_IF").Visible = false;
        this.FindControl("MnuRptPayChecking_Accounts").Visible = false;
        this.FindControl("MnuRptunpaidvoucher").Visible = false;
        this.FindControl("MnuRptPaySettlement").Visible = false;
        
    }

    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("8").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + "8" + "' And UserName='" + SessionUser + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + "8" + "' And MenuID='" + MenuID + "' And UserName='" + SessionUser + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }
    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("8").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='" + "8" + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_Menu_Form_Rights where ModuleID='" + "8" + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }

    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void btnModule_Home_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='6'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password,Fin_Year,Fin_Year_Value) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }else
            {
                Response.Redirect("/Default.aspx");
            }
        }
        else
        {
            Response.Redirect("/Default.aspx");
        }
    }
}
