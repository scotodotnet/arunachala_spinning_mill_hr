﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class SalaryApproveList : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Salary ApproveList";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
        }
        Load_Data();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
        dt.Columns.Add(new DataColumn("DOJ", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("Salary_Update_Date",typeof(string)));
        dt.Columns.Add(new DataColumn("Payroll_EmpNo", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("New_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Alllowance2", typeof(string)));
        dt.Columns.Add(new DataColumn("Alllowance2", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Salary_Type = "";
        string Approve_Status = "";

        if (chkNewWages.Checked == false && chkOldWages.Checked == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Salary Type.!');", true);
        }
        if (chkNewWages.Checked == true)
        {
            Salary_Type = "New";
        }
        else
        {
            Salary_Type = "Increment";
        }

        DataTable Sal_Ds = new DataTable();
        SSQL = "Select MachineID,ExistingCode,FirstName,DOJ,DeptName,Salary_Type,OLD_Salary,New_Salary,";
        SSQL = SSQL + "Payroll_EmpNo,Approve_Status,Convert(varchar(10),Salary_Update_Date,103) as Salary_Update_Date,Alllowance2,OLD_Alllowance2 from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Approve_Status='Approve' and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And Salary_Type='" + Salary_Type + "'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        Sal_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = Sal_Ds;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Sal_Ds;
    }

    protected void chkNewWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkNewWages.Checked == true)
        {
            chkOldWages.Checked = false;

        }
    }

    protected void chkOldWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkOldWages.Checked == true)
        {
            chkNewWages.Checked = false;
        }
    }
}
