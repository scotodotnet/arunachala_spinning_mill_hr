﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DayAttnManDaysWithPer : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                GetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
    }

  

    public void GetAttdDayWise_Change()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("DayWages");
        DataCell.Columns.Add("Wages_Type");
        DataCell.Columns.Add("Gender");


        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,EM.LastName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,LD.Total_Hrs as Tot_Hrs,";
        SSQL = SSQL + "EM.Gender,EM.Wages,EM.Adolescent from LogTime_Days LD";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
      
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " And Shift !='No Shift' And TimeIN!=''";
        
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        int Get_Actual_Present = 0;
        string Wages_Type_Gender_Join = "";
        string Wages_Type_Adolo_Join = "";
        string Adolescent_Check="0";
        int Time_In_Present = 0;
        if (AutoDTable.Rows.Count != 0)
        {
            SSQL = "Delete from Day_Attn_Record_Det_Saved where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            int sno = 1;
            Time_In_Present = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                if (Convert.ToDouble(AutoDTable.Rows[i]["Tot_Hrs"].ToString()) >= Convert.ToDouble(4))
                {
                        Get_Actual_Present = 1;
                }
                else
                {
                        Get_Actual_Present = 0;
                }


                  if((AutoDTable.Rows[i]["Wages"].ToString().ToUpper() == ("REGULAR")) || (AutoDTable.Rows[i]["Wages"].ToString().ToUpper() == ("HOSTEL")))
                  {
                    Wages_Type_Gender_Join = AutoDTable.Rows[i]["Wages"].ToString();
                    if(AutoDTable.Rows[i]["Gender"].ToString().ToUpper() == ("Female").ToUpper()) 
                    {
                        Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + AutoDTable.Rows[i]["Gender"].ToString();
                    }
                    else
                    {
                        Wages_Type_Gender_Join = Wages_Type_Gender_Join + " - " + "Male";
                    }
                  }
                  else
                  {
                    Wages_Type_Gender_Join = AutoDTable.Rows[i]["Wages"].ToString();
                  }

                if((AutoDTable.Rows[i]["Wages"].ToString().ToUpper()== ("STAFF").ToUpper()) || (AutoDTable.Rows[i]["Wages"].ToString().ToUpper() == ("Watch & Ward").ToUpper()) || (AutoDTable.Rows[i]["Wages"].ToString() == ("Manager").ToUpper()))
                {
                    Wages_Type_Adolo_Join = "STAFF";
                }
                else
                {
                    if(AutoDTable.Rows[i]["Adolescent"].ToString()=="")
                    {
                        Adolescent_Check = "0";
                        Wages_Type_Adolo_Join = AutoDTable.Rows[i]["Wages"].ToString();
                    }
                    else if (AutoDTable.Rows[i]["Adolescent"].ToString() == "1")
                    {
                        Adolescent_Check = "1";
                        if((AutoDTable.Rows[i]["Wages"].ToString().ToUpper() == ("REGULAR")) || (AutoDTable.Rows[i]["Wages"].ToString().ToUpper() == ("HOSTEL")))
                        {
                            Wages_Type_Adolo_Join = AutoDTable.Rows[i]["Wages"].ToString() + "_Adolo";
                        }
                        else
                        {
                            Wages_Type_Adolo_Join = AutoDTable.Rows[i]["Wages"].ToString();
                        }
                        
                    }
                    else
                    {
                        Adolescent_Check = "0";
                        Wages_Type_Adolo_Join = AutoDTable.Rows[i]["Wages"].ToString();
                    }
                }


                    SSQL = "Insert Into Day_Attn_Record_Det_Saved(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,Department,Wages_Type,";
                    SSQL = SSQL + "Wages_Gender_Join,Gender,Adolescent,Actual_Present,TimeINPresent,TimeIN,TimeOut,TotalHrs,Shift,Wages_Adolo_Join) Values('" + SessionCcode + "'";
                    SSQL = SSQL + ",'" + SessionLcode + "','" + AutoDTable.Rows[i]["MachineID"].ToString() + "','" + AutoDTable.Rows[i]["ExistingCode"].ToString() + "','" + AutoDTable.Rows[i]["FirstName"].ToString() + "'";
                    SSQL = SSQL + ",'" + AutoDTable.Rows[i]["LastName"].ToString() + "','" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Wages"].ToString() + "','" + Wages_Type_Gender_Join + "'";
                    SSQL = SSQL + ",'" + AutoDTable.Rows[i]["Gender"].ToString() + "','" + Adolescent_Check + "','" + Get_Actual_Present + "','" + Time_In_Present + "'";
                    SSQL = SSQL + ",'" + AutoDTable.Rows[i]["TimeIN"].ToString() + "','" + AutoDTable.Rows[i]["TimeOUT"].ToString() + "','" + AutoDTable.Rows[i]["Total_Hrs"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + Wages_Type_Adolo_Join + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    DataCell.NewRow();
                    DataCell.Rows.Add();


                    DataCell.Rows[i]["SNo"] = sno;
                    DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                    DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                    DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                    DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                    DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                    DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                    DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                    DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[i]["ShiftDate"] = Date;
                    DataCell.Rows[i]["CompanyName"] = name.ToString();
                    DataCell.Rows[i]["LocationName"] = SessionLcode;
                    DataCell.Rows[i]["LocationName"] = SessionLcode;
                    DataCell.Rows[i]["LocationName"] = SessionLcode;


                    sno += 1;

            }


            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Day_Attendance_Consolidate.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

           
            report.DataDefinition.FormulaFields["Staff_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Staff_Watch_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Regular_Male_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Regular_Female_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Hostel_Male_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Hostel_Female_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Civil_Present"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Others_Present"].Text = "'" + "0" + "'";

            DataTable mDataSet=new DataTable();
            SSQL = "Select count(Actual_Present) as Present, Wages_Gender_Join from Day_Attn_Record_Det_Saved";
            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by Wages_Gender_Join";
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if(mDataSet.Rows.Count != 0)
            {
                for(int iRow = 0;iRow< mDataSet.Rows.Count;iRow++)
                {
                    if((mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("STAFF")) || (mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("Manager").ToUpper()))
                    {
                        report.DataDefinition.FormulaFields["Staff_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("Watch & Ward").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Staff_Watch_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }

                    //'Regular
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("REGULAR - Female").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Regular_Female_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("REGULAR - Male").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Regular_Male_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }

                    //'Hostel
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("HOSTEL - Female").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Hostel_Female_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                    if (mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("HOSTEL - Male").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Hostel_Male_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                    //'Civil
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("CIVIL").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Civil_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                    //'Others
                    if(mDataSet.Rows[iRow]["Wages_Gender_Join"].ToString().ToUpper() == ("OTHERS").ToUpper())
                    {
                        report.DataDefinition.FormulaFields["Others_Present"].Text = "'" + mDataSet.Rows[iRow]["Present"] + "'";
                    }
                }
            }

            report.DataDefinition.FormulaFields["Staff_Onroll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Staff_Watch_Onroll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Regular_Female_OnRoll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Regular_male_OnRoll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Hostel_Female_OnRoll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Hostel_male_OnRoll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Civil_OnRoll"].Text = "'" + "0" + "'";
            report.DataDefinition.FormulaFields["Others_OnRoll"].Text = "'" + "0" + "'";

            //'Staff On Roll Total
             SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And (Wages='STAFF' or Wages='SUB-STAFF')  And IsActive='Yes'";
             if (Division != "-Select-")
             {
                SSQL = SSQL + " And Division = '" + Division + "'";
             } 
            if(SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if(mDataSet.Rows[0]["OnRoll_Count"].ToString()=="")
                {
                report.DataDefinition.FormulaFields["Staff_Onroll"].Text = "'" + "0" + "'";
                  
                }
                else
                {
                    report.DataDefinition.FormulaFields["Staff_Onroll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }

            //'Watch & Ward
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='Watch & Ward'  And IsActive='Yes'";
            if(SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
             {
                SSQL = SSQL + " And Division = '" + Division + "'";
             } 
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if(mDataSet.Rows[0]["OnRoll_Count"].ToString()=="")
                {
                report.DataDefinition.FormulaFields["Staff_Watch_Onroll"].Text = "'" + "0" + "'";
                  
                }
                else
                {
                    report.DataDefinition.FormulaFields["Staff_Watch_Onroll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }

              //'Regular Female On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='REGULAR' And Gender='Female' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if(mDataSet.Rows.Count != 0)
            {
                if(mDataSet.Rows[0]["OnRoll_Count"].ToString()=="")
                {
                report.DataDefinition.FormulaFields["Regular_Female_OnRoll"].Text = "'" + "0" + "'";
                  
                }
                else
                {
                    report.DataDefinition.FormulaFields["Regular_Female_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }
            //'Regular Male On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='REGULAR' And Gender='Male' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if (mDataSet.Rows[0]["OnRoll_Count"].ToString() == "")
                {
                    report.DataDefinition.FormulaFields["Regular_male_OnRoll"].Text = "'" + "0" + "'";

                }
                else
                {
                    report.DataDefinition.FormulaFields["Regular_male_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }
            //'Hostel Female On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='HOSTEL' And Gender='Female' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if (mDataSet.Rows[0]["OnRoll_Count"].ToString() == "")
                {
                    report.DataDefinition.FormulaFields["Hostel_Female_OnRoll"].Text = "'" + "0" + "'";

                }
                else
                {
                    report.DataDefinition.FormulaFields["Hostel_Female_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }
            //'Hostel Male On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='HOSTEL' And Gender='Male' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if (mDataSet.Rows[0]["OnRoll_Count"].ToString() == "")
                {
                    report.DataDefinition.FormulaFields["Hostel_male_OnRoll"].Text = "'" + "0" + "'";

                }
                else
                {
                    report.DataDefinition.FormulaFields["Hostel_male_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }
            //'CIVIL On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='CIVIL' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if (mDataSet.Rows[0]["OnRoll_Count"].ToString() == "")
                {
                    report.DataDefinition.FormulaFields["Civil_OnRoll"].Text = "'" + "0" + "'";

                }
                else
                {
                    report.DataDefinition.FormulaFields["Civil_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }

            //'Others On Roll Count\
            SSQL = "Select Count(Wages) as OnRoll_Count from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages <> 'CIVIL'";
            SSQL = SSQL + " And Wages <> 'Watch & Ward' And Wages <> 'HOSTEL' And Wages <> 'STAFF' And Wages <> 'SUB-STAFF' And Wages <> 'REGULAR' And IsActive='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mDataSet.Rows.Count != 0)
            {
                if (mDataSet.Rows[0]["OnRoll_Count"].ToString() == "")
                {
                    report.DataDefinition.FormulaFields["Others_OnRoll"].Text = "'" + "0" + "'";

                }
                else
                {
                    report.DataDefinition.FormulaFields["Others_OnRoll"].Text = "'" + mDataSet.Rows[0]["OnRoll_Count"].ToString() + "'";
                }
            }
            if (Division != "-Select-")
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Division + "'";
            }


            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        
    }
}
