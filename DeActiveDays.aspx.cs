﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class DeActiveDays : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: DeActive Days";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_WagesType();
        }
        Load_Data_DeActive();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }

    private void Load_Data_DeActive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDeActiveDays where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstDeActiveDays where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpTypeCd='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            ddlWagesType.SelectedValue = DT.Rows[0]["EmpTypeCd"].ToString();
            txtDays.Text = DT.Rows[0]["Days"].ToString();

            ddlWagesType.Enabled = false;
            btnSave.Text = "Update";
        }
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from MstDeActiveDays where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpTypeCd='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstDeActiveDays where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpTypeCd='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DeActiveDays Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_DeActive();
    }

    private void Clear_All_Field()
    {
        ddlWagesType.SelectedValue = "0";
        txtDays.Text = "";
        Load_Data_DeActive();
        ddlWagesType.Enabled = true;
        btnSave.Text = "Save";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        if (btnSave.Text == "Update")
        {
            SaveMode = "Update";

            query = "Update MstDeActiveDays set Days='" + txtDays.Text + "'";
            query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpTypeCd='" + ddlWagesType.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {

            query = "select * from MstDeActiveDays where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpTypeCd='" + ddlWagesType.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Already";
            }
            else
            {
                SaveMode = "Insert";
                query = "Insert into MstDeActiveDays (Ccode,Lcode,EmpTypeCd,EmpType,Days)";
                query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWagesType.SelectedValue + "','" + ddlWagesType.SelectedItem.Text + "','" + txtDays.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
        }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DeActiveDays Saved Successfully...!');", true);
            }
            else if (SaveMode == "Already")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DeActiveDays Already Exists!');", true);
            }
            else if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DeActiveDays Updated Successfully..!');", true);
            }
        
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
}
