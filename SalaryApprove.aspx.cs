﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class SalaryApprove : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Salary Update";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
        }
        Load_Data();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
        dt.Columns.Add(new DataColumn("DOJ", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("Payroll_EmpNo", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("New_Salary", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT=new DataTable();
        string Machine_ID_Str="";
        string Token_No_Str="";
        string Payroll_Emp_No = "";
        string[] PayrollStr;
        PayrollStr=e.CommandArgument.ToString().Split(',');
        Payroll_Emp_No = PayrollStr[0].ToString();
        Token_No_Str = PayrollStr[1].ToString();
        Machine_ID_Str = e.CommandName.ToString();

        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and Wages='" + ddlWages.SelectedItem.Text + "' And MachineID='" + Machine_ID_Str + "'";
        SSQL = SSQL + " And ExistingCode='" + Token_No_Str + "' And Payroll_EmpNo='" + Payroll_Emp_No + "'";
        SSQL = SSQL + " And (Approve_Status='Pending' or Approve_Status='Cancel' or Approve_Status='Rechecked')";
        DT=objdata.RptEmployeeMultipleDetails(SSQL);

        if(DT.Rows.Count!=0)
        {
         //Update Salary Master
       //  if(ddlWages.SelectedItem.Text.ToUpper() == ("REGULAR").ToUpper())
       //  {
       //     SSQL = "Update [Arunachalam_Payroll]..SalaryMaster set Base='" + DT.Rows[0]["New_Salary"].ToString() + "',Alllowance1amt='20.00'";
       //     SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + Payroll_Emp_No + "'";
       //  }
       // else if(ddlWages.SelectedItem.Text.ToUpper() == ("HOSTEL").ToUpper())
       //  {
       //    SSQL = "Update [Arunachalam_Payroll]..SalaryMaster set Base='" + DT.Rows[0]["New_Salary"].ToString() + "',Alllowance1amt='20.00',Deduction1='43'";
       //    SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + Payroll_Emp_No + "'";
       // }
       //else
       //  {
       //      SSQL = "Update [Arunachalam_Payroll]..SalaryMaster set Base='" + DT.Rows[0]["New_Salary"].ToString() + "'";
       //      SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + Payroll_Emp_No + "'";
       // }
       // objdata.RptEmployeeMultipleDetails(SSQL);

          //Update Base Salary IN eAlert Employee Master
            SSQL = "Update Employee_Mst set BaseSalary='" + DT.Rows[0]["New_Salary"].ToString() + "',Alllowance2='" + DT.Rows[0]["Alllowance2"].ToString() + "' where CompCode='" + SessionCcode + "'";
           SSQL = SSQL + " And LocCode='" + SessionLcode + "' And MachineID='" + Machine_ID_Str + "' And ExistingCode='" + Token_No_Str + "'";
           objdata.RptEmployeeMultipleDetails(SSQL);

           SSQL = "Update Employee_Mst_New_Emp set BaseSalary='" + DT.Rows[0]["New_Salary"].ToString() + "',Alllowance2='" + DT.Rows[0]["Alllowance2"].ToString() + "' where CompCode='" + SessionCcode + "'";
           SSQL = SSQL + " And LocCode='" + SessionLcode + "' And MachineID='" + Machine_ID_Str + "' And ExistingCode='" + Token_No_Str + "'";
           objdata.RptEmployeeMultipleDetails(SSQL);
        }          
         //Update Status
         
            SSQL = "Update Salary_Update_Det set Approve_Status='Approve',Salary_Update_Date=GetDate()";
            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And ExistingCode='" + Token_No_Str + "' And MachineID='" + Machine_ID_Str + "'";
            SSQL = SSQL + " And (Approve_Status='Pending' or Approve_Status='Cancel' or Approve_Status='Rechecked')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Successfully.!');", true);
            btnSearch_Click(sender, e);
    }

    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        string Machine_ID_Str = "";
        string Token_No_Str = "";
        string Payroll_Emp_No = "";
        string[] PayrollStr;
        PayrollStr = e.CommandArgument.ToString().Split(',');
        Payroll_Emp_No = PayrollStr[0].ToString();
        Token_No_Str = PayrollStr[1].ToString();
        Machine_ID_Str = e.CommandName.ToString();

        lblEmpNo.Text = Machine_ID_Str;
        lblTokenNo.Text = Token_No_Str;

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Salary_Type = "";
        string Approve_Status="";

        if (chkNewWages.Checked == false && chkOldWages.Checked == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Salary Type.!');", true);
        }

         if(chkNewWages.Checked == true)
         {
            Salary_Type = "New";
         }
         else
         {
             Salary_Type = "Increment";
         }

         if (ddlType.SelectedItem.Text == "Pending")
         {
            Approve_Status = "Pending";
         }
         else if (ddlType.SelectedItem.Text == "Cancel")
         {
             Approve_Status = "Cancel";
         }
         else if (ddlType.SelectedItem.Text == "Rechecked")
         {
             Approve_Status = "Rechecked";
         }
         else
         {
             Approve_Status = "";
         }

        DataTable Sal_Ds=new DataTable();
        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Approve_Status='" + Approve_Status + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And Salary_Type='" + Salary_Type + "'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        Sal_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = Sal_Ds;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Sal_Ds;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (lblEmpNo.Text != "" && lblTokenNo.Text != "" && txtReason.Text != "")
        {
            //Update Status
            SSQL = "Update Salary_Update_Det set Approve_Status='Cancel',Reason='" + txtReason.Text + "'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + lblEmpNo.Text + "'";
            SSQL = SSQL + " And ExistingCode='" + lblTokenNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Cancelled Successfully.!');", true);
            btnSearch_Click(sender, e);
            lblEmpNo.Text = "";
            lblTokenNo.Text = "";
            txtReason.Text = "";
        }
        else
        {
            if (lblEmpNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee to Cancel.!');", true);
            }
            else if (txtReason.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the reason.!');", true);
            }
        }
    }
    protected void chkNewWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkNewWages.Checked == true)
        {
            chkOldWages.Checked = false;
        }
    }

    protected void chkOldWages_CheckedChanged(object sender, EventArgs e)
    {
        if (chkOldWages.Checked == true)
        {
            chkNewWages.Checked = false;
        }
    }
}
